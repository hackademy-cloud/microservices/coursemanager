from peewee import Model, CharField, TextField, AutoField, ForeignKeyField, BooleanField, IntegerField

from courseman.database import register_model
from courseman.resources.user import User


@register_model
class StartedCourse(Model):
    id = AutoField()
    lods_id = IntegerField(null=True)
    user = ForeignKeyField(User, backref="started_courses", null=False)
    finished = BooleanField(default=False, null=False)
    url = CharField(null=True)
    expires = IntegerField(null=True)