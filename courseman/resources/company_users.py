from peewee import Model, CharField, TextField, AutoField, ForeignKeyField, ManyToManyField

from courseman.database import register_model
from courseman.resources.company import Company
from courseman.resources.user import User


@register_model
class CompanyUsers(Model):
    id = AutoField()
    company = ForeignKeyField(Company, null=False, backref="users")
    user = ForeignKeyField(User, null=False, backref="companies")
