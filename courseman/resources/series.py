from peewee import Model, CharField, TextField, AutoField, ForeignKeyField, IntegerField

from courseman.database import register_model


@register_model
class Series(Model):
    id = AutoField(null=False)
    lods_id = IntegerField(null=True, unique=True)
    name = CharField(null=True)
    description = TextField(null=True)
    difficulty = IntegerField(null=True)  # between 0 and 10