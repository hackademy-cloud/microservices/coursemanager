from peewee import Model, CharField, TextField, AutoField, ForeignKeyField, BooleanField, DateField

from courseman.database import register_model
from courseman.resources.series import Series
from courseman.resources.company import Company


@register_model
class PayedSeries(Model):
    id = AutoField()
    company = ForeignKeyField(Company, backref="payed_series")
    series = ForeignKeyField(Series, backref="payed_companies")
    activated = BooleanField(default=False)