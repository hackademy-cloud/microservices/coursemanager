from peewee import Model, CharField, TextField, AutoField, ForeignKeyField, DeferredForeignKey, IntegerField, \
    DoesNotExist, BooleanField

from courseman.application.environment import oidc, keycloak_client, keycloak_admin
from courseman.database import register_model
from courseman.oidc_utils.dataclasses import UserInfo


@register_model
class User(Model):
    """The user model saves the user in the database. It gets the main userinformation from the oauth system, but it's
    possible to add additional information to the user here. The data saved here can be outdated and needs to be
    synchronised with oauth. The information saved here works a little bit like a cache."""
    oidc_id = CharField(primary_key=True, null=False)
    deleted = BooleanField(null=False, default=False)
    preferred_username = CharField(null=True)
    name = CharField(null=True)
    given_name = CharField(null=True)
    family_name = CharField(null=True)
    email = CharField(null=True)

    def update_userinfo(self, user: UserInfo):
        """Updates the information about a user (only if something has changed)."""
        equals = True
        if self.preferred_username != user.preferred_username:
            self.preferred_username = user.preferred_username
            equals = False
        if self.name != user.name:
            self.name = user.name
            equals = False
        if self.given_name != user.given_name:
            self.given_name = user.given_name
            equals = False
        if self.family_name != user.family_name:
            self.family_name = user.family_name
            equals = False
        if self.email != user.email:
            self.email = user.email
            equals = False
        if not equals:
            user.save()

    @classmethod
    def my_get_by_id(cls, pk):
        """Loads a user from the database and updates the information with the infos from oauth.

        If there are no userinfo in oauth, the user will be deactivated. If the user doesn't exist in the local
        database the user will be created with the retrieved information from oauth.

        pk: the user id from oauth."""
        userinfo = keycloak_admin.get_user(pk)
        user = cls.get_or_none(pk)
        if userinfo is None:
            # deactivate user
            if user is not None:
                user.deleted = True
                user.save()
            raise DoesNotExist
        if user is None:
            user = cls.create(oidc_id=userinfo.id,
                              deleted=False,
                              preferred_username=userinfo.preferred_username,
                              name=userinfo.name,
                              given_name=userinfo.given_name,
                              family_name=userinfo.family_name,
                              email=userinfo.email)
            user.save()
            return user
        if user.oidc_id != userinfo.id:
            raise Exception("userids are not equal. unknown reason!")
        user.update_userinfo(userinfo)
        return user

    @classmethod
    def my_get_by_id_or_none(cls, pk):
        """Loads a user and updates it's information."""
        try:
            return cls.my_get_by_id(pk)
        except DoesNotExist:
            return None
