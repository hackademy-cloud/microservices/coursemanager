from peewee import Model, CharField, TextField, AutoField, ForeignKeyField, IntegerField, BooleanField

from courseman.database import register_model
from courseman.resources.series import Series


@register_model
class Course(Model):
    id = AutoField()
    lods_id = IntegerField(null=True)
    name = CharField(null=True)
    description = TextField(null=True)
    objective = TextField(null=True)
    scenario = TextField(null=True)
    enabled = BooleanField(default=False, null=False)
    reason_disabled = TextField(null=True)
    expected_duration = IntegerField(null=True)  # Time in minutes
    maximum_duration = IntegerField(null=True)  # Time in minutes
    series = ForeignKeyField(Series, null=True, backref="courses")