from peewee import Model, CharField, TextField, AutoField, ForeignKeyField, ManyToManyField

from courseman.database import register_model
from courseman.resources.user import User


@register_model
class Company(Model):
    id = AutoField()
    name = CharField(null=False, unique=True)
    owner = ForeignKeyField(User, null=False, backref="owned_companies")
