import argparse
from dataclasses import dataclass
from typing import Optional

from courseman.version import VERSION


@dataclass(frozen=True)
class ArgsConfig:
    lods_api_key: Optional[str] = None
    flask_host: str = "127.0.0.1"
    flask_port: int = 8080
    db_file: str = "volume/database.sqlite"


def _parse_args():
    parser = argparse.ArgumentParser(
        description="Adds an api to manage courses.")
    parser.add_argument("-v",
                        "--version",
                        help="show version",
                        action="version",
                        version=VERSION)
    # configs
    parser.add_argument("--lods-api-key",
                        help="Learn on demand systems API key",
                        action="store",
                        default=ArgsConfig.lods_api_key)
    parser.add_argument("--flask-host",
                        nargs="?",
                        help="host where the api is running",
                        action="store",
                        default=ArgsConfig.flask_host)
    parser.add_argument("--flask-port",
                        nargs="?",
                        help="port where the api is running",
                        action="store",
                        default=ArgsConfig.flask_port)
    parser.add_argument("--db-file",
                        nargs="?",
                        help="location of the sqlite database file",
                        action="store",
                        default=ArgsConfig.db_file)
    return parser.parse_args()


def create_args_config() -> ArgsConfig:
    """Creates the default args config from commandline input."""
    args = _parse_args()
    return ArgsConfig(args.lods_api_key, args.flask_host, int(args.flask_port), args.db_file)