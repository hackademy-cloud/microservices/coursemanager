from courseman.config.env_vars_loader import load_env


class EnvConfig:
    """All environment configuration that is not included in the app config."""
    def __init__(self, environ):
        self.keycloak_example_1_username: str = environ["KEYCLOAK_EXAMPLE_1_USERNAME"]
        self.keycloak_example_1_password: str = environ["KEYCLOAK_EXAMPLE_1_PASSWORD"]
        self.keycloak_example_2_username: str = environ["KEYCLOAK_EXAMPLE_2_USERNAME"]
        self.keycloak_example_2_password: str = environ["KEYCLOAK_EXAMPLE_2_PASSWORD"]


def create_env_config():
    env = load_env()
    return EnvConfig(environ=env)