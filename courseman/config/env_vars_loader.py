import os

from dotenv import load_dotenv

_loaded_env = False


def load_env():
    """Loads the environment variables from .env or from the environment
    if the file doesn't exist, but only once"""
    global _loaded_env
    if _loaded_env:
        return os.environ
    _basedir = os.path.abspath(os.path.dirname(__file__))
    _file = os.path.join(_basedir, '../../.env')
    if os.path.isfile(_file):
        print("Loading config from file")
        load_dotenv(_file)
    else:
        print("Loading config from environment variables")
    _loaded_env = True
    return os.environ


