from __future__ import annotations
from typing import Optional
import argparse
from dataclasses import dataclass

from courseman.config.args_config import ArgsConfig, create_args_config
from courseman.config.env_config import EnvConfig, create_env_config
from courseman.version import VERSION


class Config:
    __config: Optional[Config] = None

    def __init__(self, args_config: ArgsConfig, env_config: EnvConfig):
        self.args_config = args_config
        self.env_config = env_config

    @staticmethod
    def create_manual(args_config: ArgsConfig, env_config: EnvConfig):
        Config.__config = Config(args_config, env_config)

    @staticmethod
    def get():
        if Config.__config is None:
            Config.__config = Config(create_args_config(), create_env_config())
        return Config.__config
