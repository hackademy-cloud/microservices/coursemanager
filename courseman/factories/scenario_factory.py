from lods_lib.datamodel.lab_series import LabSeries

from courseman.resources.series import Series


def _try_update(series: Series, lab_series: LabSeries):
    changed = False
    if series.name != lab_series.Name:
        series.name = lab_series.Name
        changed = True
    if series.description != lab_series.Description:
        series.description = lab_series.Description
        changed = True
    if series.description != lab_series.Description:
        series.description = lab_series.Description
        changed = True
    if changed:
        series.save()


def create_series_from_lab_series(lab_series: LabSeries) -> Series:
    lods_id = lab_series.Id
    series = Series.get_or_none(lods_id=lods_id)
    if series is not None:
        _try_update(series, lab_series)
        return series
    name = lab_series.Name
    description = lab_series.Description
    series = Series.create(lods_id=lods_id, name=name, description=description)
    return series
