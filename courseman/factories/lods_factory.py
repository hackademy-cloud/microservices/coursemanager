from lods_lib import LodsApiClient

from courseman.factories.course_factory import create_course_from_lab_profile
from courseman.factories.scenario_factory import create_series_from_lab_series
from courseman.utils.debug import debug_print


def update_catalog(lods_client: LodsApiClient):
    debug_print("updating courses")
    catalog = lods_client.catalog()
    debug_print(catalog)
    for series_single in catalog.LabSeries:
        debug_print("eine serie")
        debug_print(series_single)
        create_series_from_lab_series(series_single)
    for profile in catalog.LabProfiles:
        debug_print("ein profile")
        debug_print(profile)
        create_course_from_lab_profile(profile)
