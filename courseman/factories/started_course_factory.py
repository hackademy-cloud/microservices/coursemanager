from lods_lib.api_methods.lab_profile import LabProfileResponse
from lods_lib.api_methods.launch import LaunchResponse
from lods_lib.api_methods.launch_for_event import LaunchForEventResponse
from lods_lib.api_methods.launch_for_event_by_external_id import LaunchForEventByExternalIDResponse
from lods_lib.datamodel.lab_instance import LabInstance
from lods_lib.datamodel.lab_profile import LabProfile
from typing import Union

from lods_lib.datamodel.running_lab import RunningLab

from courseman.resources.course import Course
from courseman.resources.started_course import StartedCourse


LaunchUnion = Union[RunningLab, LaunchResponse, LaunchForEventResponse,
                    LaunchForEventByExternalIDResponse]


def _try_update(started_course: StartedCourse, launch: LaunchUnion):
    changed = False
    mapped = [("expires", "Expires"),
              ("url", "Url")]
    for course_key, profile_key in mapped:
        if started_course.__dict__[course_key] != launch.__dict__[profile_key]:
            started_course.__dict__[course_key] = launch.__dict__[profile_key]
            changed = True
    if changed:
        started_course.save()


def create_started_course_from_lab_instance(launch: LaunchUnion) -> StartedCourse:
    lods_id = launch.LabInstanceId
    started_course = StartedCourse.get_or_none(lods_id=lods_id)
    if started_course is not None:
        _try_update(started_course, launch)
        return started_course
    started_course = StartedCourse.create(lods_id=lods_id, expires=launch.Expires,
                                  url=launch.Url)
    return started_course
