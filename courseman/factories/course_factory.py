from lods_lib.api_methods.lab_profile import LabProfileResponse
from lods_lib.datamodel.lab_profile import LabProfile
from typing import Union

from courseman.resources.course import Course
from courseman.resources.series import Series


def _try_update(course: Course, lab_profile: Union[LabProfile, LabProfileResponse]):
    changed = False
    mapped = [("name", "Name"),
              ("description", "Description"),
              ("objective", "Objective"),
              ("scenario", "Scenario"),
              ("reason_disabled", "ReasonDisabled"),
              ("expected_duration", "ExpectedDurationMinutes"),
              ("maximum_duration", "DurationMinutes")]
    for course_key, profile_key in mapped:
        if getattr(course, course_key) != getattr(lab_profile, profile_key):
            setattr(course, course_key, getattr(lab_profile, profile_key))
            changed = True
    enabled = lab_profile.Enabled
    if enabled is None:
        enabled = False
    if enabled != course.enabled:
        course.enabled = enabled
        changed = True
    if course.series is None or course.series.lods_id != lab_profile.SeriesId:
        course.series = Series.get_or_none(lods_id=lab_profile.SeriesId)
        changed = True
    if changed:
        course.save()


def create_course_from_lab_profile(lab_profile: Union[LabProfile, LabProfileResponse]) -> Course:
    lods_id = lab_profile.Id
    course = Course.get_or_none(lods_id=lods_id)
    if course is not None:
        _try_update(course, lab_profile)
        return course
    enabled = lab_profile.Enabled
    if enabled is None:
        enabled = False
    series = Series.get_or_none(lods_id=lab_profile.SeriesId)
    course = Course.create(lods_id=lods_id, name=lab_profile.Name,
                           description=lab_profile.Description,
                           objective=lab_profile.Objective,
                           scenario=lab_profile.Scenario,
                           enabled=enabled,
                           reason_disabled=lab_profile.ReasonDisabled,
                           expected_duration=lab_profile.ExpectedDurationMinutes,
                           maximum_duration=lab_profile.DurationMinutes,
                           series=series)
    return course
