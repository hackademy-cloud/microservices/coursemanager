from courseman.application.environment import app


def bad_data(error):
    return "400 Bad Request", 400


def page_not_found(error):
    return "404 Not Found", 404


def method_not_allowed(error):
    return "405 Method Not Allowed", 405


def unprocessable_entity(error):
    """The 422 (Unprocessable Entity) status code means the server
    understands the content type of the request entity, and the
    syntax of the request entity is correct but was unable to process
    the contained instructions.
    """
    return "422 Unprocessable Entity", 422


def internal_server_error(error):
    return "500 Internal Server Error", 500


def add_errorhandlers():
    app.errorhandler(400)(bad_data)
    app.errorhandler(404)(page_not_found)
    app.errorhandler(405)(method_not_allowed)
    app.errorhandler(422)(unprocessable_entity)
    app.errorhandler(500)(internal_server_error)
