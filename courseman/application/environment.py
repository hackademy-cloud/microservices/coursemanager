from flask import Flask
from flask_marshmallow import Marshmallow
from flask_restful import Api

from courseman.application.app_config import load_app_config
from courseman.oidc_utils.keycload_client import KeycloakClient, KeycloakAdmin
from courseman.oidc_utils.oidc_wrapper import OpenIDConnectWrapper

app = Flask(__name__)
api = Api()
ma = Marshmallow()
oidc = OpenIDConnectWrapper()
keycloak_client = KeycloakClient()
keycloak_admin = KeycloakAdmin()


def init():
    """Initializes the environment"""
    load_app_config(app)
    api.init_app(app)
    ma.init_app(app)
    keycloak_client.init_app(app)
    keycloak_admin.init_app(app)
    oidc.init_app(app)
