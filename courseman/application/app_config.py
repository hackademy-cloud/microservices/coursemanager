import os

from flask import Flask

from courseman.config.env_vars_loader import load_env
from courseman.oidc_utils.client_secrets_writer import ClientSecretsWriter
from courseman.utils.converter import str_to_bool
from courseman.utils.debug import debug_print


def _load_app_config(flask_app: Flask, environ):
    """Loads the app config from environ.

    Args:
        flask_app: The app where the config should be loaded
        environ: Should be os.environ
    """
    flask_app.config.update({
        "TESTING": str_to_bool(environ.get("TESTING", "False")),
        "DEBUG": str_to_bool(environ.get("DEBUG", "False")),
        "OIDC_ID_TOKEN_COOKIE_SECURE": str_to_bool(environ.get("OIDC_ID_TOKEN_COOKIE_SECURE", "True")),
        "KEYCLOAK_SERVER_URL": environ["KEYCLOAK_SERVER_URL"],
        "KEYCLOAK_REALM_NAME": environ["KEYCLOAK_REALM_NAME"],
        "KEYCLOAK_CLIENT_ID": environ["KEYCLOAK_CLIENT_ID"],
        "SECRET_KEY": environ["SECRET_KEY"],
    })


def _init_oidc(flask_app):
    """Creates the oidc client secrets file and changes the app
    config to work with oidc.

    Needs to be executed, after _load_app_config"""
    _basedir = os.path.abspath(os.path.dirname(__file__))
    _filename = '../../client_secrets.json'
    _file = os.path.join(_basedir, _filename)
    flask_app.config.update({
        'OIDC_OPENID_REALM': flask_app.config["KEYCLOAK_CLIENT_ID"],
        'OIDC_CLIENT_SECRETS': _file,
        'OIDC_REQUIRE_VERIFIED_EMAIL': False,
        'OIDC_USER_INFO_ENABLED': True,
        'OIDC_SCOPES': ['openid', 'email', 'profile'],
        'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
        'OIDC_TOKEN_TYPE_HINT': 'access_token'
    })
    writer = ClientSecretsWriter(keycloak_url=flask_app.config["KEYCLOAK_SERVER_URL"],
                                 realm=flask_app.config["KEYCLOAK_REALM_NAME"],
                                 client_id=flask_app.config["KEYCLOAK_CLIENT_ID"],
                                 client_secret=flask_app.config["SECRET_KEY"])
    writer.write_file(flask_app.config["OIDC_CLIENT_SECRETS"])


def load_app_config(flask_app):
    env = load_env()
    debug_print(env)
    _load_app_config(flask_app=flask_app, environ=env)
    _init_oidc(flask_app=flask_app)
