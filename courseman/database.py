from __future__ import annotations
from typing import List, Type
from peewee import SqliteDatabase, Model, Proxy, DeferredForeignKey

from courseman.config.config import Config


class Database:

    __instance = Proxy()

    @staticmethod
    def get() -> Proxy:
        return Database.__instance

    @staticmethod
    def initialize(database: Database):
        Database.__instance.initialize(database)

    @staticmethod
    def initialize_sqlite():
        # TODO db.connect() vor jedem befehl
        config = Config.get().args_config
        database = SqliteDatabase(config.db_file)
        Database.__instance.initialize(database)

    @staticmethod
    def initialize_default():
        Database.initialize_sqlite()


class Schema:
    __list: List[Model] = []

    @staticmethod
    def register(model: Model):
        Schema.__list.append(model)

    @staticmethod
    def __get_all() -> List[Model]:
        return Schema.__list

    @staticmethod
    def create_tables():
        for table in Schema.__list:
            DeferredForeignKey.resolve(table)
        Database.get().create_tables(Schema.__list)


def register_model(cls: Type[Model]):
    Schema.register(cls)
    cls._meta.database = Database.get()
    return cls

