import json


class ClientSecretsWriter:
    def __init__(self, keycloak_url: str, realm: str, client_id: str, client_secret: str):
        if keycloak_url.endswith("/"):
            # remove slash
            keycloak_url = keycloak_url[:-1]
        self.keycloak_url = keycloak_url
        self.realm = realm
        self.client_id = client_id
        self.client_secret = client_secret

    def get_client_secrets_string(self) -> str:
        client_secret = {
            "web": {
                "client_id": self.client_id,
                "client_secret": self.client_secret,
                "redirect_uris": [
                    "*"   # TODO
                ],
                "issuer": "{keycloak_url}/realms/{realm}".format(keycloak_url=self.keycloak_url, realm=self.realm),
                "auth_uri": "{keycloak_url}/realms/{realm}/protocol/openid-connect/auth".format(keycloak_url=self.keycloak_url, realm=self.realm),
                "userinfo_uri": "{keycloak_url}/realms/{realm}/protocol/openid-connect/userinfo".format(keycloak_url=self.keycloak_url, realm=self.realm),
                "token_uri": "{keycloak_url}/realms/{realm}/protocol/openid-connect/token".format(keycloak_url=self.keycloak_url, realm=self.realm),
                "token_introspection_uri": "{keycloak_url}/realms/{realm}/protocol/openid-connect/token/introspect".format(keycloak_url=self.keycloak_url, realm=self.realm)
            }
        }
        return json.dumps(client_secret)

    def write_file(self, filename: str) -> None:
        secrets_file = open(filename, "w")
        secrets_str = self.get_client_secrets_string()
        secrets_file.write(secrets_str)
        secrets_file.close()