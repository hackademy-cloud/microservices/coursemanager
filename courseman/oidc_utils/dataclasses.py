from typing import Dict, Any, Optional, List


class UserInfo:
    """Dataclass for keycloak_openid.userinfo(token.access_token)"""

    def __init__(self, json: Dict[str, Any]):
        self.id: str = json["sub"]
        self.preferred_username: Optional[str] = json["preferred_username"]
        self.email_verified: bool = json.get("email_verified")
        self.name: Optional[str] = json.get("name")
        self.given_name: Optional[str] = json.get("given_name")
        self.family_name: Optional[str] = json.get("family_name")
        self.email: Optional[str] = json.get("email")

    @staticmethod
    def create_from_admin(json: Dict[str, Any]):
        json["sub"] = json.get("id")
        json["preferred_username"] = json.get("username")
        json["given_name"] = json.get("firstName")
        json["family_name"] = json.get("lastName")
        json["email_verified"] = json.get("emailVerified")
        return UserInfo(json)

    def to_str(self):
        return str(self.__dict__)


class KeycloakUserToken:
    """Dataclass for keycloak_openid.token(username, password)"""

    def __init__(self, json: Dict[str, Any]):
        self.access_token: Optional[str] = json.get("access_token")
        self.refresh_token: Optional[str] = json.get("refresh_token")
        self.expires_in: Optional[int] = json.get("expires_in")
        self.refresh_expires_in: Optional[int] = json.get("refresh_expires_in")
        self.token_type: Optional[str] = json.get("token_type")
        self.not_before_policy: Optional[int] = json.get("not_before_policy")
        self.session_state: Optional[str] = json.get("session_state")
        self.scope: Optional[str] = json.get("scope")


class OidcTokenInfo(UserInfo):
    """Dataclass for g.oidc_token_info"""

    def __init__(self, obj):
        self.acr: str = obj.get("acr")
        self.active: bool = obj.get("active")
        self.allowed_origins: List[Any] = obj.get("allowed-origins")
        self.aud: str = obj.get("aud")
        self.auth_time: int = obj.get("auth_time")
        self.azp: str = obj.get("azp")
        self.client_id: str = obj.get("client_id")
        self.exp: int = obj.get("exp")
        self.iat: int = obj.get("iat")
        self.iss: str = obj.get("iss")
        self.jti: str = obj.get("jti")
        self.scope: str = obj.get("scope")
        self.session_state: str = obj.get("session_state")
        self.typ: str = obj.get("typ")
        realm_access = obj.get("realm_access", {})
        self.realm_access_roles = realm_access.get("roles", [])
        resource_access = obj.get("resource_access", {})
        account = resource_access.get("account", {})
        self.resource_access_account_roles = account.get("roles", [])
        super().__init__(obj)