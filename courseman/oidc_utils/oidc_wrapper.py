from flask import Flask
from flask_oidc import OpenIDConnect
from typing import Optional

from courseman.oidc_utils.dataclasses import UserInfo


class OpenIDConnectWrapper(OpenIDConnect):
    def __init__(self, app: Optional[Flask]=None):
        super().__init__(app)

    def userinfo(self):
        user_info = super().user_getinfo(['preferred_username', 'email', 'sub', 'email_verified', 'name', 'given_name', 'family_name'])
        return UserInfo(user_info)
