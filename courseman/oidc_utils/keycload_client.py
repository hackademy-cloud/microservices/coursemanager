from flask import Flask
from keycloak import KeycloakOpenID, KeycloakAdmin as KeycloakAdminOpenID
from typing import Optional

from courseman.oidc_utils.dataclasses import UserInfo, KeycloakUserToken


class KeycloakAdmin():
    def __init__(self, app: Optional[Flask]=None):
        if app is None:
            self.keycloak_admin: Optional[KeycloakAdminOpenID] = None
        else:
            self.init_app(app)

    def init_app(self, app: Flask):
        self.keycloak_admin = KeycloakAdminOpenID(server_url=app.config["KEYCLOAK_SERVER_URL"],
                                                  client_id=app.config["KEYCLOAK_CLIENT_ID"],
                                                  realm_name=app.config["KEYCLOAK_REALM_NAME"],
                                                  client_secret_key=app.config["SECRET_KEY"],
                                                  verify=True)

    def _check_init(self):
        if self.keycloak_admin is None:
            raise Exception("KeycloakAdmin is not initialized")

    def get_user(self, user_id):
        self._check_init()
        user = self.keycloak_admin.get_user(user_id)
        return (UserInfo.create_from_admin(user))


class KeycloakClient():
    def __init__(self, app: Optional[Flask]=None):
        if app is None:
            self.keycloak_openid: Optional[KeycloakOpenID] = None
        else:
            self.init_app(app)

    def init_app(self, app: Flask):
        self.keycloak_openid = KeycloakOpenID(server_url=app.config["KEYCLOAK_SERVER_URL"],
                                              client_id=app.config["KEYCLOAK_CLIENT_ID"],
                                              realm_name=app.config["KEYCLOAK_REALM_NAME"],
                                              client_secret_key=app.config["SECRET_KEY"])

    def _check_init(self):
        if self.keycloak_openid is None:
            raise Exception("KeycloakClient is not initialized")

    def login(self, username, password):
        self._check_init()
        token = self.keycloak_openid.token(username, password)
        return KeycloakUserToken(token)

    def userinfo(self, token: KeycloakUserToken) -> UserInfo:
        self._check_init()
        userinfo = self.keycloak_openid.userinfo(token.access_token)
        return UserInfo(userinfo)

    def logout(self, token: KeycloakUserToken):
        self._check_init()
        self.keycloak_openid.logout(token.refresh_token)


