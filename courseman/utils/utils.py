from flask import abort
from typing import Type, Optional, Union, Dict

from flask_marshmallow import Schema
from flask_restful.reqparse import RequestParser
from peewee import Model

from courseman.utils.debug import debug_print


class NoResponseSchema:
    def dump(self, _):
        return None, 204


no_response_schema = NoResponseSchema


MySchema = Optional[Union[Schema, NoResponseSchema]]


def setattr_parser(obj, parser_args) -> bool:
    """Returns True if something was changed."""
    change = False
    for arg in parser_args:
        if arg is not None:
            debug_print("Update {}: {}={} (Before: {})".format(obj, arg, parser_args[arg], obj[arg]))
            setattr(obj, arg, parser_args[arg])
            change = True
    return change


def dump(schema: MySchema, obj):
    if schema is None:
        return obj
    return schema.dump(obj)


def get_all(model: Type[Model], schema: MySchema = None):
    objs = model.select()
    return dump(schema, objs)


def get_or_abort(model: Type[Model], schema: MySchema = None, **kwargs):
    obj = model.get_or_none(**kwargs)
    if obj is None:
        abort(404)
    return dump(schema, obj)


def patch_or_abort(model: Type[Model], attributes: Dict[str, str],
                   schema: MySchema = None, **kwargs):
    obj = get_or_abort(model, **kwargs)
    if setattr_parser(obj, attributes):
        obj.save()
        return dump(schema, obj)
    abort(400)
