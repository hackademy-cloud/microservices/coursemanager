from distutils import util


def str_to_bool(string: str) -> bool:
    return bool(util.strtobool(string))