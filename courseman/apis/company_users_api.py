from flask_restful import Resource

from courseman.application.environment import api
from courseman.resources.company_users import CompanyUsers
from courseman.schemas.company_schema import private_companies_schema, private_company_schema
from courseman.utils.utils import get_or_abort, get_all


# not testet; don't know if it works
class CompanyUsersListAPI(Resource):
    def get(self):
        return get_all(CompanyUsers, schema=private_companies_schema)


class CompanyUsersAPI(Resource):
    def get(self, company_id: int):
        return get_or_abort(CompanyUsers, schema=private_company_schema, id=company_id)


def init():
    api.add_resource(CompanyUsersListAPI, '/company_user/<int:company_user_id>', endpoint='company')
