from flask_restful import reqparse, Resource, abort
from lods_lib import LodsApiClient
from peewee import JOIN

from courseman.application.environment import api
from courseman.factories.lods_factory import update_catalog
from courseman.resources.course import Course
from courseman.resources.series import Series
from courseman.schemas.series_schema import public_short_series_many_schema, public_extended_series_many_schema, \
    public_extended_series_schema, public_short_series_schema


class SeriesListAPI(Resource):
    def __init__(self, lods_client: LodsApiClient):
        self.lods_client = lods_client

    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('extended', required=False, type=bool, default=False,
                            help='If the output should be extended.')
        args = parser.parse_args()
        update_catalog(self.lods_client)
        series = Series.select(Series, Course).join(Course, JOIN.LEFT_OUTER)
        if args.pop("extended"):
            return public_extended_series_many_schema.dump(series)
        return public_short_series_many_schema.dump(series)


class SeriesAPI(Resource):
    def __init__(self, lods_client: LodsApiClient):
        self.lods_client = lods_client

    def get(self, series_id: int):
        parser = reqparse.RequestParser()
        parser.add_argument('extended', required=False, type=bool, default=False,
                            help='If the output should be extended.')
        args = parser.parse_args()
        update_catalog(self.lods_client)
        series = Series.select(Series, Course).join(Course, JOIN.LEFT_OUTER).where(Series.id == series_id).limit(1)
        if len(series) < 1:
            abort(404)
        if args.pop("extended"):
            return public_extended_series_schema.dump(series[0])
        return public_short_series_schema.dump(series[0])


def init(lods_client: LodsApiClient):
    api.add_resource(SeriesListAPI, '/series',
                     endpoint ='series_many',
                     resource_class_args=[lods_client])
    api.add_resource(SeriesAPI, '/series/<int:series_id>',
                     endpoint ='series',
                     resource_class_args=[lods_client])
