from flask_restful import reqparse, Resource

from courseman.application.environment import api
from courseman.resources.company import Company
from courseman.resources.user import User
from courseman.schemas.company_schema import public_company_schema, public_companies_schema
from courseman.utils.utils import get_or_abort, patch_or_abort, get_all


class CompanyListAPI(Resource):
    def get(self):
        return get_all(Company, schema=public_companies_schema)

    def post(self):
        """Register"""
        parser = reqparse.RequestParser()
        parser.add_argument('name', required=True, type=str, help='The name of the company')
        parser.add_argument('owner_id', required=True, type=int, help='The id of the owner')
        args = parser.parse_args()
        args["owner"] = get_or_abort(User, id=args.pop("owner_id"))
        company = Company.create(**args)
        return public_company_schema.dump(company)


class CompanyAPI(Resource):
    def get(self, company_id: int):
        return get_or_abort(Company, schema=public_company_schema, id=company_id)

    def patch(self, company_id: int):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, help='The name of the company')
        parser.add_argument('owner_id', type=str, help='The id of the owner')
        args = parser.parse_args()
        args["owner"] = get_or_abort(User, id=args.pop("owner_id"))
        return patch_or_abort(Company, args, schema=public_company_schema, id=company_id)

    def delete(self, company_id: int):
        get_or_abort(Company, id=company_id)
        Company.delete().where(Company.id == company_id).execute()
        return None, 204


def init():
    api.add_resource(CompanyListAPI, '/company', endpoint='companies')
    api.add_resource(CompanyAPI, '/company/<int:company_id>', endpoint='company')
