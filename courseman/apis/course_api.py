from flask_restful import Resource
from lods_lib import LodsApiClient
from courseman.application.environment import api
from courseman.factories.lods_factory import update_catalog
from courseman.resources.course import Course
from courseman.schemas.course_schema import public_course_schema, public_courses_schema
from courseman.utils.utils import get_all, get_or_abort


class CourseListAPI(Resource):
    def __init__(self, lods_client: LodsApiClient):
        self.lods_client = lods_client

    def get(self):
        update_catalog(self.lods_client)
        return get_all(Course, schema=public_courses_schema)


class CourseAPI(Resource):
    def __init__(self, lods_client: LodsApiClient):
        self.lods_client = lods_client

    def get(self, course_id: int):
        update_catalog(self.lods_client)
        return get_or_abort(Course, schema=public_course_schema, id=course_id)


def init(lods_client: LodsApiClient):
    api.add_resource(CourseListAPI, '/course',
                     endpoint ='courses',
                     resource_class_args=[lods_client])
    api.add_resource(CourseAPI, '/course/<int:course_id>',
                     endpoint ='course',
                     resource_class_args=[lods_client])
