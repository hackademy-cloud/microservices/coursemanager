from courseman.application.environment import keycloak_client, app, oidc, init
from flask import g
import requests
import json

from courseman.config.config import Config
from courseman.oidc_utils.dataclasses import OidcTokenInfo
from courseman.resources.user import User


def oauth():
    ma = [Config.get().env_config.keycloak_example_1_username,
             Config.get().env_config.keycloak_example_1_password]
    mo = [Config.get().env_config.keycloak_example_2_username,
           Config.get().env_config.keycloak_example_2_password]
    token = keycloak_client.login(*mo)
    print(keycloak_client.userinfo(token).to_str())
    keycloak_client.logout(token)


def oauth_admin():
    ma = [Config.get().env_config.keycloak_example_1_username,
          Config.get().env_config.keycloak_example_1_password]
    mo = [Config.get().env_config.keycloak_example_2_username,
          Config.get().env_config.keycloak_example_2_password]
    token = keycloak_client.login(*mo)
    print(keycloak_client.userinfo(token).to_str())
    user_id = keycloak_client.userinfo(token).id
    keycloak_client.logout(token)
    user = User.my_get_by_id(user_id)
    print(user.oidc_id)
    print(user.preferred_username)


@app.route('/')
def hello_world():
    if oidc.user_loggedin:
        return ('Hello, %s, <a href="/private">See private</a> '
                '<a href="/logout">Log out</a>') % \
               oidc.user_getfield('preferred_username')
    else:
        return 'Welcome anonymous, <a href="/private">Log in</a>'


@app.route("/greeting")
@oidc.require_login
def greeting():
    user_info = oidc.userinfo()
    print(user_info.to_str())
    greet = "Hello {}".format(user_info.preferred_username)
    return greet


@app.route('/private')
@oidc.require_login
def hello_me():
    """Example for protected endpoint that extracts private information from the OpenID Connect id_token.
       Uses the accompanied access_token to access a backend service.
    """
    user_info = oidc.userinfo()

    if user_info.id in oidc.credentials_store:
        try:
            from oauth2client.client import OAuth2Credentials
            access_token = OAuth2Credentials.from_json(oidc.credentials_store[user_info.id]).access_token
            print ('access_token=<%s>' % access_token)
            headers = {'Authorization': 'Bearer %s' % (access_token)}
            # example of executing the api with a token
            greeting = requests.get('http://127.0.0.1:5000/greeting', headers=headers).text
        except:
            print ("Could not access greeting-service")
            greeting = "Hallo {}".format(user_info.preferred_username)
    else:
        greeting = "Hallo {}".format(user_info.preferred_username)

    return ("""%s your email is %s and your user_id is %s!
               <ul>
                 <li><a href="/">Home</a></li>
                </ul>""" %
            (greeting, user_info.email, user_info.id))


@app.route('/api', methods=['POST'])
@oidc.accept_token(require_token=True)
def hello_api():
    """OAuth 2.0 protected API endpoint accessible via AccessToken"""
    oidc_token_info = OidcTokenInfo(g.oidc_token_info)
    print(oidc_token_info.to_str())
    return json.dumps({'hello': 'Welcome %s' % g.oidc_token_info['sub']})


@app.route("/login")
@oidc.require_login
def login():
    "Method to get the curl command with loggedin access token"
    access_token = oidc.get_access_token()
    headers = 'curl -X POST -d "access_token={}" localhost:5000/'.format(access_token)
    return headers


@app.route('/logout')
def logout():
    """Performs local logout by removing the session cookie."""
    oidc.logout()
    return 'Hi, you have been logged out! <a href="/">Return</a>'


def main():
    init()
    oauth()
    oauth_admin()
    app.run()
