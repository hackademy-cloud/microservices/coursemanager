from flask_restful import reqparse, Resource, abort
from courseman.application.environment import api
from courseman.resources.user import User
from courseman.schemas.user_schema import public_user_schema, public_users_schema
from courseman.utils.utils import get_or_abort, patch_or_abort, get_all, dump


class UserListAPI(Resource):
    def get(self):
        return get_all(User, schema=public_users_schema)

    def post(self):
        """Register"""
        parser = reqparse.RequestParser()
        parser.add_argument('display_name', required=True, type=str, help='The display name of the user')
        args = parser.parse_args()
        user = User.create(**args)
        return public_user_schema.dump(user)


class UserAPI(Resource):
    def get(self, user_id: int):
        obj = User.my_get_by_id_or_none(user_id)
        if obj is None:
            abort(404)
        return dump(public_user_schema, obj)

    def patch(self, user_id: int):
        parser = reqparse.RequestParser()
        parser.add_argument('display_name', type=str, help='The display name of the user')
        args = parser.parse_args()
        return patch_or_abort(User, args, schema=public_user_schema, id=user_id)

    def delete(self, user_id: int):
        get_or_abort(User, id=user_id)
        User.delete().where(User.id == user_id).execute()
        return None, 204


def init():
    api.add_resource(UserListAPI, '/user', endpoint ='users')
    api.add_resource(UserAPI, '/user/<string:user_id>', endpoint ='user')
