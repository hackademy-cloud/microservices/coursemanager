from courseman.application.environment import ma


class PublicUserSchema(ma.Schema):
    class Meta:
        fields = ("oidc_id", "preferred_username")
        # add company url


class PrivateUserSchema(ma.Schema):
    class Meta:
        fields = ("oidc_id", "preferred_username")


public_user_schema = PublicUserSchema()
public_users_schema = PublicUserSchema(many=True)
private_user_schema = PrivateUserSchema()
private_users_schema = PrivateUserSchema(many=True)