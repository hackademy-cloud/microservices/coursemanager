from marshmallow import fields

from courseman.application.environment import ma


class PublicCourseSchema(ma.Schema):
    series_id = fields.Function(lambda obj: obj.series.id)

    class Meta:
        fields = ("id", "name", "description", "objective", "scenario",
                  "enabled", "reason_disabled", "expected_duration",
                  "maximum_duration", "series_id")


public_course_schema = PublicCourseSchema()
public_courses_schema = PublicCourseSchema(many=True)
