from marshmallow import fields

from courseman.schemas.user_schema import public_user_schema, public_users_schema
from courseman.application.environment import ma


class PublicCompanySchema(ma.Schema):
    class Meta:
        fields = ("id", "name")


class PrivateCompanySchema(ma.Schema):
    owner = fields.Function(lambda obj: public_user_schema.dump(obj.owner))
    users = fields.Function(lambda obj: public_users_schema.dump(obj.users))

    class Meta:
        fields = ("id", "name", "owner", "users")


public_company_schema = PublicCompanySchema()
public_companies_schema = PublicCompanySchema(many=True)
private_company_schema = PrivateCompanySchema()
private_companies_schema = PrivateCompanySchema(many=True)