from marshmallow import fields

from courseman.application.environment import ma


class _CustomCourseSchema(ma.Schema):
    class Meta:
        fields = ("id", "name", "description", "objective", "scenario",
                  "enabled", "reason_disabled", "expected_duration",
                  "maximum_duration")


_custom_courses_schema = _CustomCourseSchema(many=True)


class PublicExtendedSeriesSchema(ma.Schema):
    courses = fields.Function(lambda obj: _custom_courses_schema.dump(obj.courses))

    class Meta:
        fields = ("id", "name", "description", "difficulty", "courses")


class PublicShortSeriesSchema(ma.Schema):
    course_ids = fields.Function(lambda obj: [course.id for course in obj.courses])

    class Meta:
        fields = ("id", "name", "description", "difficulty", "course_ids")


public_extended_series_schema = PublicExtendedSeriesSchema()
public_extended_series_many_schema = PublicExtendedSeriesSchema(many=True)
public_short_series_schema = PublicShortSeriesSchema()
public_short_series_many_schema = PublicShortSeriesSchema(many=True)