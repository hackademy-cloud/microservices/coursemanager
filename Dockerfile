FROM python:3.8-alpine

WORKDIR /app

RUN apk --no-cache add python3-dev
COPY requirements.txt /
RUN pip install --no-cache-dir -r /requirements.txt && rm /requirements.txt
RUN apk del python3-dev

COPY . /app

VOLUME /volume

EXPOSE 80

CMD [ "python", "/app/run", "--db-file=/volume/database.sqlite", "--flask-host=0.0.0.0", "--flask-port=80"]
