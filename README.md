

# Coursemanager
Dieses Programm soll die Kurse irgendwo speichern und über eine API die yml-Files oder deren Text verfügbar machen.

## Archived Project
This project is just a prototype and now archived. To see the active and working solution take a look at this project: https://gitlab.com/hackademy-cloud/microservices/coursemanager2

# API
## Course
Kurse werden automatisch von LearnOnDemand Systems ausgelesen und in der Api gespeichert.

### Methods
- [x] `GET /course`: Gibt alle Kurse zurück
- [x] `GET /course/{id}`: Gibt einen Kurs zurück

### Create, Update, Delete
- Über LearnOnDemand Systems

## Series
Serien werden automatisch von LearnOnDemand Systems ausgelesen und in der Api gespeichert.

### Methods
- [x] `GET /series`: Gibt alle Serien zurück
- [x] `GET /series/{id}`: Gibt eine Serie zurück
- [x] `GET /series?extended=True`: Gibt alle Serien zurück und die dazu gehörigen Kurse
- [x] `GET /series/{id}?extended=True`: Gibt eine Serie zurück und die dazu gehörigen Kurse

### Create, Update, Delete
- Über LearnOnDemand Systems

## User
### TODO:
- [ ] Einbinden von OAuth
- [ ] Beim Löschen von Usern auch Company löschen?
- [ ] Beim Löschen von Company auch User löschen?

### Methods
- [x] `GET /user`: Gibt alle User zurück
- [x] `GET /user/{id}`: Gibt einen User zurück
- [x] `POST /user`: Fügt einen User hinzu. Needed post field: `display_name`.
- [x] `PATCH /user/{id}`: Updated einen User. Available post field: `display_name`.
- [x] `DELETE /user/{id}`: Löscht den Nutzer.
- [ ] `GET /user`: Integriert in Keycloak. (Aktuell, gibt es nur die cached User aus.)
- [x] `GET /user/{id}`: Integriert in Keycloak. (Sollte funktionieren)
- [ ] `POST /user`: Integriert in Keycloak.
- [ ] `PATCH /user/{id}`: Integriert in Keycloak.
- [ ] `DELETE /user/{id}`: Integriert in Keycloak.

## Company
### TODO:
- [ ] Einbinden von OAuth?
- [ ] Beim Löschen von Usern auch Company löschen?
- [ ] Beim Löschen von Company auch User löschen?

### Methods
- [x] `GET /company`: Gibt alle Companies zurück
- [x] `GET /company/{id}`: Gibt eine Company zurück
- [x] `POST /company`: Fügt eine Company hinzu. Needed post field: `name` and `owner_id`.
- [x] `PATCH /company/{id}`: Updated eine Company. Available post field: `name` and `owner_id`.
- [x] `DELETE /company/{id}`: Löscht die Company.

# Args Config
See `./run --help` for more information
