import setuptools
import os


VERSION = "0.0.1"
TESTING_VERSION = os.environ.get("TESTING_VERSION")

if TESTING_VERSION is not None:
    VERSION += ".dev{}".format(TESTING_VERSION)

setuptools.setup(
    name="coursemanager",
    version=VERSION,
    author="Marco Schlicht",
    author_email="m.schlicht@hackademy.cloud",
    description="Api for Hackademy",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8.2',
)